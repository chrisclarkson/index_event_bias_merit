# index_event_bias_merit

Merit has an LD clumping function which would be useful to implement before using index event bias correction.
Here I've used the `rpy2` module to combine Frank Dudbrge's code with merit.
# test
```
python clump_and_index_event_bias.py --prognosis CVD_diseased_Triglycerides_int_tested_tmp.txt.gz --incidence CVD_non-diseased_Triglycerides_int_tested_tmp.txt.gz
```

Firstly you need to install these 2 packages:


https://gitlab.com/cfinan/merit


https://gitlab.com/SchmidtAF/merit_helper
```
python -m pip install --upgrade -r requirements.txt
python -m pip install -e .
index_event_bias_merit --help
```
Input files (examples are made avaiable) should be formatted as such (column name options are available if not like below- see help output):
```
chr     pos     af      rsid    a_0     a_1     beta    se      pval
1       10177   0.397557        rs367896724     A       AC      0.025198        0.016467        0.125991138387356
1       10352   0.39242 rs201106462     T       TA      -0.025296       0.016892        0.134264129360973
1       11008   0.086133        rs575272151     C       G       0.023532        0.028228        0.404501372927709
1       11012   0.086133        rs544419019     C       G       0.023532        0.028228        0.404501372927709
```
Example run to perform clumping and IEB on sumstats files:
```
cd ~/index_event_bias_merit
f='CVD'
bio="Triglycerides"
index_event_bias_merit --incidence ${f}_non-diseased_${bio}_int_tested_tmp.txt.gz --prognosis ${f}_diseased_${bio}_int_tested_tmp.txt.gz --output ~/Scratch/gwas_project/${f}/${f}_${bio}_ie_tmp.txt.gz
```
If the clumping process is taking too long to do in one job, you can choose to just clump one chromosome at a time in different jobs and perform ieb later:
```
index_event_bias_merit --incidence ... --prognosis ... --operations clump --chr 1 --output chr1_clumped.txt.gz .... 

....
#concattenate all separate chromosome data into one file with pre-pruned variants

index_event_bias_merit --operations ieb --prepruned_list_variants prepruned.txt.gz

```

NOTE:
Clumping is performed by subsetting the chromosome in a sliding window (1MB is the default) and performing LD calculations on each subset.
If you want to perform clumping while moving across the chromosome in batches of SNPs (e.g. 1000 bp at a time) you can do this with the flags:
```
--iterative_window 'per_snp' --window 1000
```
