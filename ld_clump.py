from math import exp
from merit import dataset
from merit import constants as c
from merit import data_tools, refpops, analysis_tools
from merit.ensembl import rest
from merit import mendelian_randomization as mr
from merit import merit_errors
from merit.prs import scores
import pandas as pd

def ld_clump(exposure,maf=0.01,ldcut=[0.1],logpval=0.0001,ld_path="None",maf_path="None",ref_pop='/lustre/projects/ICS_UKB/config_files/gen.cnf'):
    ref2 = scores.read_ref_pop_file(ref_pop)
    print('here')
    if ld_path == 'None' or maf_path == 'None':
        # Sample size of 1000 is sufficient for MAF of 0.05 or higher
        df_dict = {}
        for c in exposure.chr_name.unique():
            exp_chr = exposure[exposure.chr_name == c]
            Plist = scores.gwas_ref_corr_matrix(exp_chr, refs=ref2,
                seed=1986, sample_size=1000, verbose=True)
            if len(Plist) == 0:
                print('Chromosome ' + str(c) + ': ' + str(exp_chr.shape[0]) + ' variants')
                continue
            ### looping over the chromosomes
            k = Plist.keys()
            P = pd.concat([values[1] for k, values in Plist.items()])
            ### MAF
            MAF = P.effect_allele_freq.copy()
            MAF.loc[MAF > 0.5] = 1-MAF[MAF > 0.5]
            # filtering
            maf_pass = MAF[MAF >= maf].index
            exp_chr = exp_chr.loc[maf_pass].copy()
            if exp_chr.shape[0]==0:
                print('maf filterer out variants')
            # all removed take next chr
            if exp_chr.shape[0] == 0:
                print('Chromosome ' + str(c) + ': ' + str(exp_chr.shape[0]) + ' variants')
                continue
            ### GETTING LD
            L = [values[2] for k, values in Plist.items()]
            ld_matrix = refpops.get_ld_matrix(L[0])
            # only filter if there is one single float
            # (mr_wrap will iterate instead)
            # And if doing genomewide MR (just to increase speed).
            if (len(ldcut) == 1) and not ('coordinates' in locals() or 'coordinates' in globals()):
                ld_matrix = ld_matrix.loc[exp_chr.index, exp_chr.index].copy()
            # not sure why this happens?!
                if not ld_matrix.shape[0] == exp_chr.shape[0]:
                    keepuniq = ld_matrix.index.duplicated()
                    ld_matrix = ld_matrix.loc[~keepuniq, ~keepuniq].copy()
                    keepuniq2 = exp_chr.index.duplicated()
                    exp_chr = exp_chr.loc[~keepuniq2].copy()
                    del keepuniq, keepuniq2
                # normal run
                exp_chr = analysis_tools.filter_ld_clump(exp_chr, ld_matrix,
                    rsquare=ldcut[0], index_col='pvalue',
                    index_ascending=logpval)
                ld_matrix = ld_matrix.loc[exp_chr.index, exp_chr.index].copy()
                df_dict[c] = exp_chr
            else:
                df_dict[c] = exp_chr
            print('Chromosome ' + str(c) + ': ' + str(exp_chr.shape[0]) + ' variants')
            del maf_pass, k, P, Plist
            del exp_chr
        # end of for loop (overwriting exposure, pruned subset)
        exposure = pd.concat(df_dict.values(), ignore_index=False)
        return exposure

