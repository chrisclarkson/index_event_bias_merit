from math import exp
from merit import dataset
from merit import constants as c
from merit import data_tools, refpops, analysis_tools
from merit.ensembl import rest
from merit import mendelian_randomization as mr
from merit import merit_errors
from merit.prs import scores
from ld_clump import ld_clump

import rpy2.robjects as robjects
import rpy2.robjects.packages as packages
from rpy2.robjects.vectors import StrVector
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr
utils = importr('utils')
pandas2ri.activate()
import pandas as pd
import numpy as np
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('--incidence',
    type=str,
    help='healthy file',
    default='~/Scratch/gwas_project/CVD/CVD_non-diseased_Triglycerides_int_tested_tmp.txt.gz'
    )
parser.add_argument('--prognosis',
    type=str,
    help='diseased file',
    default='~/Scratch/gwas_project/CVD/CVD_diseased_Triglycerides_int_tested_tmp.txt.gz'
    )
parser.add_argument('--output',
    type=str,
    help='output file',
    default='ie.txt'
    )
parser.add_argument('--chr_name',
    type=str,
    help='chromosome col',
    default='chr'
    )
parser.add_argument('--start_pos',
    type=str,
    help='position col',
    default='pos'
    )
parser.add_argument('--effect_allele',
    type=str,
    help='effect allele col',
    default='a_1'
    )
parser.add_argument('--other_allele',
    type=str,
    help='other allele col',
    default='a_0'
    )
parser.add_argument('--rsid',
    type=str,
    help='rsid col',
    default='rsid'
    )
parser.add_argument('--effect_size',
    type=str,
    help='effect size col',
    default='beta'
    )
parser.add_argument('--pvalue',
    type=str,
    help='pval col',
    default='pval'
    )
parser.add_argument('--standard_error',
    type=str,
    help='SE col',
    default='se'
    )
args = parser.parse_args()

def prepare_for_IE(file,ld_clump_or_not=True):
    data_merit=dataset.read_gwas(file,
        'beta', effect_allele=args.effect_allele, other_allele=args.other_allele,
        chr_name=args.chr_name, start_pos=args.start_pos,
        pvalue=args.pvalue,standard_error=args.standard_error,effect_size=args.effect_size,
        compression='gzip',chunksize=10000, sep='\t' ,drop_reserved = True)
    if ld_clump_or_not:
        data_merit=ld_clump(data_merit)
    cols_dict={args.effect_allele:'effect_allele', args.other_allele:'other_allele',
        args.chr_name:'chr_name', args.start_pos:'start_pos',
        args.pvalue:'pvalue',args.standard_error:'standard_error',args.effect_size:'effect_size'}
    data=pd.read_csv(file,sep='\t')
    data.rename(columns=cols_dict)
    print(data)
    data['uni_id']=data['chr_name'].astype(str)+'_'+data['start_pos'].astype(str)+'_'+data['other_allele']+'_'+data['effect_allele']
    data=data.loc[data['uni_id'].isin(data_merit.index)]
    return data

incidence=prepare_for_IE(args.incidence)
prognosis=prepare_for_IE(args.prognosis,ld_clump_or_not=False)
prognosis=prognosis.loc[prognosis['uni_id'].isin(incidence['uni_id'])]

r = robjects.r
r['source']('index_event_bias_py.R')
index_event_bias=robjects.globalenv['index_event_bias']
ie_data=index_event_bias(incidence,prognosis)
print(ie_data)
ie_data.to_csv(args.output,sep='\t',index=False)

