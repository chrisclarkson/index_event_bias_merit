from math import exp
from merit import dataset
from merit import constants as c
from merit import data_tools, refpops, analysis_tools
from merit.ensembl import rest
from merit import mendelian_randomization as mr
from merit import merit_errors
from merit.prs import scores
from numpy.lib.shape_base import expand_dims
from ld_clump import ld_clump

import rpy2.robjects as robjects
import rpy2.robjects.packages as packages
from rpy2.robjects.vectors import StrVector
from rpy2.robjects import pandas2ri
from rpy2.robjects.packages import importr
from rpy2.robjects.conversion import localconverter
import re
import sys
import os
import subprocess
utils = importr('utils')
pandas2ri.activate()
import pandas as pd
import numpy as np
import argparse

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser()

parser.add_argument('--incidence',
    type=str,
    help='healthy file',
    default='/home/rmgpccl/Scratch/gwas_project/CVD/CVD_non-diseased_Triglycerides_int_tested_tmp.txt.gz'
    )
parser.add_argument('--prognosis',
    type=str,
    help='diseased file',
    default='/home/rmgpccl/Scratch/gwas_project/CVD/CVD_diseased_Triglycerides_int_tested_tmp.txt.gz'
    )
parser.add_argument('--output',
    type=str,
    help='output file',
    default='ie.txt'
    )
parser.add_argument('--chr_name',
    type=str,
    help='chromosome col',
    default='chr'
    )
parser.add_argument('--start_pos',
    type=str,
    help='position col',
    default='pos'
    )
parser.add_argument('--effect_allele',
    type=str,
    help='effect allele col',
    default='a_0'
    )
parser.add_argument('--other_allele',
    type=str,
    help='other allele col',
    default='a_1'
    )
parser.add_argument('--rsid',
    type=str,
    help='rsid col',
    default='rsid'
    )
parser.add_argument('--effect_size',
    type=str,
    help='effect size col',
    default='beta'
    )
parser.add_argument('--pvalue',
    type=str,
    help='pval col',
    default='pval'
    )
parser.add_argument('--r2',
    type=float,
    help='r2 threshold',
    default=0.001
    )
parser.add_argument('--standard_error',
    type=str,
    help='SE col',
    default='se'
    )
parser.add_argument('--chr',
    type=str,
    help='chr',
    default=None
    )
parser.add_argument('--window',
    type=int,
    help='clumping window',
    default=1000000
    )
parser.add_argument('--iterative_window',
    type=str,
    help='style to iterate the data "per_bp" (default) or "per_snp"',
    default='per_bp'
    )
parser.add_argument('--ieb_method',
    type=str,
    help='Use Simex or Hedges-Olkin',
    default='Simex'
    )
parser.add_argument('--operations',
    type=str,
    nargs='+',
    help='input the terms "clump" and "ieb" or both to specify which you would like done. Default is to do both',
    default=['clump','ieb']
    )
parser.add_argument('--prepruned_list_variants',
    type=str,
    help='file formatted the same way as the prognosis/incidence with list of prepruned variants',
    default=None
    )
parser.add_argument('--refpop_file',
    type=str,
    help='reference population file',
    default='/lustre/projects/ICS_UKB/config_files/gen.cnf'
    )
parser.add_argument('--revert_to_old_colnames',
    type=str2bool,
    help='rename the columns of output to original input',
    default=True
    )
parser.add_argument('--path_to_mr_pipelines',
    type=str,
    help='the path to "merit_helper/pipelines" which is the module on which this package relies',
    default=None
    )

args = parser.parse_args()

r = robjects.r #load R with py2R
rfile=os.path.abspath('./index_event_bias_py2.R')
r['source'](rfile)
index_event_bias=robjects.globalenv['index_event_bias']

if args.path_to_mr_pipelines is not None:
    if os.environ['MERIT_HELPER_PIPELINES']:
        print('Overwriting path to merit_helper/pipelines from {0} to {1}'.format(os.environ['MERIT_HELPER_PIPELINES'],args.path_to_mr_pipelines))
    with open(os.path.expanduser("{0}/.bashrc".format(os.environ['HOME'])), "a") as outfile:
        path_reset='export MERIT_HELPER_PIPELINES="{0}"'.format(args.path_to_mr_pipelines)
        outfile.write(path_reset)
    subprocess.Popen(path_reset, shell=True).wait()
    sys.path.insert(1,os.path.join(os.environ['HOME'],args.path_to_mr_pipelines))
    from run_mr import get_ldandclump
else:
    print('Using path to merit_helper/pipelines from {0}'.format(os.environ['MERIT_HELPER_PIPELINES']))
    sys.path.insert(1,os.path.join(os.environ['HOME'],os.environ['MERIT_HELPER_PIPELINES']))
    from run_mr import get_ldandclump

def read_ref_pop_file(fn):
    ref_pop_map = {}
    with open(fn) as infile:
        for i in infile:
            i = i.strip()
            chromo, path = re.split(r'[\s\t]+', i, maxsplit=1)
            ref_pop_map[chromo] = path
    for i in ref_pop_map.keys():
        if int(i)<10:
            ref_pop_map[str(int(i))]=ref_pop_map.pop(i)
    return ref_pop_map


def subset_data(chr_sub,gen_cnf,r2):
    try:
        chr_sub,ld_object=get_ldandclump(exposure=chr_sub,exposure_all=None,
            cohort_file=gen_cnf,ensemblid='genomewide',
            ld_sample=1000,logpval=False,model='univariable',
            exp=None,proximity=False)
        chr_sub = analysis_tools.filter_ld_clump(chr_sub, ld_object,
                rsquare=r2, index_col='pvalue',
                index_ascending=1e-06)
    except:
        print('no variants')
    return chr_sub


def prepare_for_IE(data,chr,iterative_window='per_bp',r2=0.001):
    gen_cnf=read_ref_pop_file(args.refpop_file)
    data_merit=dataset.set_gwas(data,
        'beta', effect_allele=args.effect_allele, other_allele=args.other_allele,
        chr_name=args.chr_name, start_pos=args.start_pos,
        pvalue=args.pvalue,standard_error=args.standard_error,effect_size=args.effect_size)
    if chr is not None:
        data_merit = data_merit[data_merit.chr_name == str(chr)].copy()
    full_chr=pd.DataFrame()
    window=args.window
    for c in data_merit.chr_name.unique():
        print(c)
        exp_chr = data_merit[data_merit.chr_name == c].copy()
        if iterative_window=='per_bp':
            for idx in range(exp_chr['start_pos'].values[0],max(exp_chr['start_pos'])-window,window):    
                chr_sub=exp_chr.loc[(exp_chr['start_pos']>idx) & (exp_chr['start_pos']<=idx+window)].copy()
                chr_sub=subset_data(chr_sub,gen_cnf,r2=r2)
                full_chr=full_chr.append(chr_sub)
        elif iterative_window=='per_snp':
            as_far_as=(exp_chr.shape[0]+1)-window
            for idx in range(0,as_far_as,window):
                chr_sub=exp_chr.iloc[idx:idx+window]
                chr_sub=subset_data(chr_sub,gen_cnf,r2=r2)
                full_chr=full_chr.append(chr_sub)
    return full_chr

def translate_header(data,direction='forwards'):
    if direction=='forwards':
        cols_dict={args.effect_allele:'effect_allele', args.other_allele:'other_allele',
            args.chr_name:'chr_name', args.start_pos:'start_pos',
            args.pvalue:'pvalue',args.standard_error:'standard_error', args.effect_size:'effect_size'}
        data=data.rename(columns=cols_dict) #change colnames so that they are consistent with merit and suitable for R function
    else:
        cols_dict={'effect_allele':args.effect_allele, 'other_allele':args.other_allele,
            'chr_name':args.chr_name, 'start_pos':args.start_pos,
            'pvalue':args.pvalue,'standard_error':args.standard_error, 'effect_size':args.effect_size}
        data=data.rename(columns=cols_dict)
    return data

print(args.operations)
def main():
    prognosis=pd.read_csv(args.prognosis,sep='\t') #read prognosis
    if args.prepruned_list_variants is None and 'clump' in args.operations:
        pruned=prepare_for_IE(prognosis,chr=args.chr,iterative_window=args.iterative_window,r2=args.r2) #prune prognosis
        pruned['uni_id']=pruned['chr_name'].astype(str)+'_'+pruned['start_pos'].astype(str)+'_'+pruned['effect_allele']+'_'+pruned['other_allele']
    else:
        if args.prepruned_list_variants is not None:
            if 'clump' in args.operations:
                print('since a file with prepruned variants is provided clumping will not be done')
            pruned=pd.read_csv(args.prepruned_list_variants,sep='\t') #read pruned list
            pruned=translate_header(pruned,direction='forwards')
            if 'uni_id' not in pruned.columns:
                pruned['uni_id']=pruned['chr_name'].astype(str)+'_'+pruned['start_pos'].astype(str)+'_'+pruned['effect_allele']+'_'+pruned['other_allele']
        else:
            print('print no clumping just IEB to be done...')
            pruned=0
    prognosis=translate_header(prognosis,direction='forwards')
    prognosis['uni_id']=prognosis['chr_name'].astype(str)+'_'+prognosis['start_pos'].astype(str)+'_'+prognosis['effect_allele']+'_'+prognosis['other_allele']
    incidence=pd.read_csv(args.incidence,sep='\t') #read incidence
    incidence=translate_header(incidence,direction='forwards')
    incidence['uni_id']=incidence['chr_name'].astype(str)+'_'+incidence['start_pos'].astype(str)+'_'+incidence['effect_allele']+'_'+incidence['other_allele']
    pruned['uni_id']=pruned['chr_name'].astype(str)+'_'+pruned['start_pos'].astype(str)+'_'+pruned['effect_allele']+'_'+pruned['other_allele']
    if 'ieb' in args.operations:
        r = robjects.r #load R with py2R
        rfile=os.path.abspath('./index_event_bias_py2.R')
        r['source'](rfile)
        index_event_bias=robjects.globalenv['index_event_bias'] #create and load R function
        if pruned!=0:
            ie_data=index_event_bias(incidence,prognosis,args.ieb_method,pruned['uni_id']) #perform IEB
        else:
            ie_data=index_event_bias(incidence,prognosis,args.ieb_method,0)
        if args.revert_to_old_colnames: #if columns need to be converted back to original input format
            ie_data=translate_header(ie_data,direction='backwards')
    else:
        ie_data=pruned
    ie_data.to_csv(args.output,sep='\t',index=False,compression='gzip')

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        sys.stderr.write("User interrupted!")
        sys.exit(0)

